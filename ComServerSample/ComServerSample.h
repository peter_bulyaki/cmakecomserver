// ComServerSample.h : Declaration of module class.
#include "resource.h"

class CComServerSampleModule : public ATL::CAtlDllModuleT< CComServerSampleModule >
{
public :
    DECLARE_LIBID(LIBID_ComServerSampleLib)
    DECLARE_REGISTRY_APPID_RESOURCEID(IDR_ComServerSample, "{CE807033-6BEE-44D3-A86A-E9BC1D0716A4}")
};

extern class CComServerSampleModule _AtlModule;
